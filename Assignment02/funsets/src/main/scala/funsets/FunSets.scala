package funsets

import common._


/*
 * Coursera Online Course:
 * Functional Programming Principles in Scala
 * @author Melih Degis
 * Assignment-02
 * Purely Functional Sets
 *
 * TODO:
 * 
 * set syntax'ında bi kaç hata var sanırım compile edilirken
 * hata veriyor.
 *
 * filter, forall, exists ve map eksik
 *
 * test dosyasına bikaç test eklencek
 * */

object FunSets {
  /**
   * We represent a set by its characteristic function, i.e.
   * its `contains` predicate.
   */
  type Set = Int => Boolean

  /**
   * Indicates whether a set contains a given element.
   */
  def contains(s: Set, elem: Int): Boolean = s(elem)

  /**
   * Returns the set of the one given element.
   * 
   * tek elemanlı set
   */
  def singletonSet(elem: Int): Set = Set(elem)

  /**
   * Returns the union of the two given sets,
   * the sets of all elements that are in either `s` or `t`.
   * 
   * birleşim yani or işlemi
   */
  def union(s: Set, t: Set): Set = s || t

  /**
   * Returns the intersection of the two given sets,
   * the set of all elements that are both in `s` or `t`.
   * 
   * kesişim yani and işlemi
   */
  def intersect(s: Set, t: Set): Set = s && t

  /**
   * Returns the difference of the two given sets,
   * the set of all elements of `s` that are not in `t`.
   * 
   * çıkartma: ilk küme kesişim ikincinin tersi
   */
  def diff(s: Set, t: Set): Set = s && !t

  /**
   * Returns the subset of `s` for which `p` holds.
   *
   * anlamadım ?!.
   */
  def filter(s: Set, p: Int => Boolean): Set = ???

  /**
   * The bounds for `forall` and `exists` are +/- 1000.
   */
  val bound = 1000

  /**
   * Returns whether all bounded integers within `s` satisfy `p`.
   */
  def forall(s: Set, p: Int => Boolean): Boolean = {
    def iter(a: Int): Boolean = {
      if (???) ???
      else if (???) ???
      else iter(???)
    }
    iter(???)
  }

  /**
   * Returns whether there exists a bounded integer within `s`
   * that satisfies `p`.
   */
  def exists(s: Set, p: Int => Boolean): Boolean = ???

  /**
   * Returns a set transformed by applying `f` to each element of `s`.
   */
  def map(s: Set, f: Int => Int): Set = ???

  /**
   * Displays the contents of a set
   */
  def toString(s: Set): String = {
    val xs = for (i <- -bound to bound if contains(s, i)) yield i
    xs.mkString("{", ",", "}")
  }

  /**
   * Prints the contents of a set on the console.
   */
  def printSet(s: Set) {
    println(toString(s))
  }
}
