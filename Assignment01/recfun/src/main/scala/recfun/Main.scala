package recfun
import common._

/*
 * Coursera Online Course:
 * Functional Programming Principles in Scala
 * @author Melih Degis
 * Assignment-01
 *
  * balance sorusundaki son test patlıyor hocam
  * bi tek o kaldı, yardım edin mehmet aliiğğ beeğğ
 * */

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   * same with scheme implementation 
   */
  def pascal(c: Int, r: Int): Int = {
    if(c==0 || c==r) 1
    else pascal(c-1, r-1)+pascal(c,r-1)
  }

  /**
   * Exercise 2
   * balancing problem
   * iteravite way does not work
   * and I have no idea why
   * TODO: find the problem
   * FIX: do not use iteravite way.

  def balance(chars: List[Char]): Boolean = {
    var x =0
    for(i <- 0 to chars.length){
	if(chars(i)=='(')
	   x=x+1
	else if(chars(i)==')')
	   x=x-1
    }
    if(x==0)
      return true
    else 
      return false
  }
   */

/*
* Oppan Scheme Style!
* ERROR AGAIN!!!!
* ())(
* counting parenthesis is not enough,
* also I need to check localization 
 * */
 
  def balance(chars: List[Char]): Boolean = {
     def inner(chars: List[Char], temp:Int): Boolean = {
       if(chars.isEmpty) temp == 0
       else 
	 {
	   if(chars.head=='(') inner(chars.tail, temp + 1)
	   else if(chars.head==')') inner(chars.tail, temp - 1)
	   else inner(chars.tail, temp)}
     }
    inner(chars, 0)
}

  /**
   * Exercise 3
   * tail recursive
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    if(money==0 || coins.isEmpty) 0
    else if(money==0) 1
    else 
      countChange(money, coins.tail) +
      countChange(money - coins.head, coins)
  }
}
